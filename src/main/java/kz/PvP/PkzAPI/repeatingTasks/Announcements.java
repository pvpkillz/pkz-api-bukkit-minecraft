package kz.PvP.PkzAPI.repeatingTasks;

import java.util.HashMap;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.scheduler.BukkitRunnable;




public class Announcements extends BukkitRunnable {

	static Main plugin;
	int MessageID = 0;
	public static HashMap<String, String> MessagesModify = new HashMap<String, String>();

    public Announcements() {

    }

	@Override
	public void run() {
		if (MessageID >= Main.Announcements.size())
			MessageID = 0;
		
		String message = Main.Announcements.get(MessageID);
		Message.G(message, true);
		
		MessageID++;
	}
}
