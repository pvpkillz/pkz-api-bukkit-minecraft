package kz.PvP.PkzAPI.repeatingTasks;

import kz.PvP.PkzAPI.Main;
import kz.PvP.PkzAPI.enums.ScoreboardUtility;
import kz.PvP.PkzAPI.methods.Scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;




public class ScoreboardUpdater extends BukkitRunnable {

	static Main plugin;

	public ScoreboardUpdater() {

	}

	@Override
	public void run() {
		
		for (final Player p : Bukkit.getOnlinePlayers()){
			Scoreboards.updateScoreboardEntries(p, Main.scoreBoardTitles, null);
		}
	}
}
