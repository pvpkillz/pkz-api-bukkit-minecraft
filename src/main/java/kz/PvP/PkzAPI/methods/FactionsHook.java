package kz.PvP.PkzAPI.methods;

import java.util.logging.Logger;

import kz.PvP.PkzAPI.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.massivecraft.factions.entity.UPlayer;

public class FactionsHook implements Listener{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public FactionsHook (Main mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	public static String getFactionName(String name) {
		UPlayer uplayer = UPlayer.get(PlayersInfo.getPlayer(name));
		String FactionName = uplayer.getFaction().getName();
		if (!ChatColor.stripColor(FactionName).toLowerCase().contains("wilderness"))
			FactionName = uplayer.getRole().getPrefix() + FactionName + " ";
		else
			FactionName = "";
		return FactionName;
	}
	
	public static UPlayer getFactionPlayer(Player p){
		UPlayer uplayer = UPlayer.get(p);
		return uplayer;
	}
	
}
